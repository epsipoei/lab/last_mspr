# Last_mspr

## Vagrant

* Install
  * <https://developer.hashicorp.com/vagrant/install>

* Plugins

  * VirtualBox:

    ``` bash
    vagrant plugin install vagrant-vbguest
    ```
  * Libvirt with kvm:  
    - Install KVM: <https://ubuntu.com/blog/kvm-hyphervisor>
      ```bash
      sudo apt -y install bridge-utils cpu-checker libvirt-clients libvirt-daemon qemu qemu-kvm
      ```
    - Install plugin to vagrant: <https://vagrant-libvirt.github.io/vagrant-libvirt/>
      ``` bash
      sudo apt install libvirt-dev
      sudo apt-get install build-essential 
      vagrant plugin install vagrant-libvirt
      ```
    - Install nfs-kernel-server: <https://packages.debian.org/fr/sid/nfs-kernel-server>
      ```bash
      sudo apt-get install nfs-kernel-server
      sudo systemctl enable nfs-kernel-server
      ```