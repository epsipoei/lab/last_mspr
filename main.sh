#!/bin/bash
parent_directory=$(dirname "$(pwd)")

GREEN='\033[0;32m'
YELLOW='\033[0;33m'
MAGENTA='\033[0;35m'
CYAN='\033[0;36m'
NC='\033[0m'

### Project List ###
projects=("elk"
          "bastion"
          "sql"
          "haproxy"
          "samba"
          "monitoring"
          "glpi"
          "Return"
          )
projects_elk=("elasticsearch_1"
              "elasticsearch_2"
              "kibana"
              "Return"
              )
projects_bastion=("guacamole"
                  "teleport"
                  "Return"
                )
projects_sql=("maria_1"
              "maria_2"
              "Return"
            )
projects_haproxy=("haproxy_1"
                  "haproxy_2"
                  "Return"
                )
projects_samba=("samba_1"
                "samba_2"
                "Return"
                )
projects_monitoring=("grafana"
                     "influxdb"
                     "Return"
                     )

### Display Main Menu ###
print_main_menu() {
    echo "+--------------+"
    echo -e "| ${CYAN}Last Project${NC} |"
    echo "+--------------+"

    echo -e "\n${GREEN}Choice:${NC}\n"
    echo "1) Vagrant"
    echo "2) SSH"
    echo "3) Ansible"
    echo "4) Exit"

    printf "\n${GREEN}Input Your choice:${NC} " 
    read choice

    case $choice in
        1|Vagrant)
            clear
            vagrant_menu
        ;;
        2|SSH)
            clear
            ssh_menu
        ;;
        3|Ansible)
            clear
            ansible_menu
        ;;
        4|Exit)
            exit 1
        ;;
        *)
            echo -e "\nInvalid choice. Please try again.\n"
        ;;
    esac
} 
return_menu() {
    echo -e "\n${GREEN}Return to the main menu? (y/n):${NC} "
    read rep
}

### VAGRANT ###
title_vagrant_menu() {
    echo "+--------------+"
    echo -e "| ${CYAN}Menu Vagrant${NC} |"
    echo "+--------------+"
}
vagrant_command() {
    local project=$1
    clear
    title_vagrant_menu
    echo -e "\n${GREEN}Choice:${NC}\n"
    echo "1) Destroy"
    echo "2) Up"
    echo "3) return"
    printf "\n${GREEN}Input Your choice:${NC} " 
    read choice
    case $choice in
        1|Destroy)
            clear
            cd $parent_directory/last_mspr/vagrant/$project
            vagrant destroy
            echo -e "\n${GREEN}Return to the main menu? (y/n):${NC} "
            read rep
        ;;    
        2|Up)
            clear
            cd $parent_directory/last_mspr/vagrant/$project
            vagrant up
            echo -e "\n${GREEN}Return to the main menu? (y/n):${NC} "
            read rep
        ;;
        3|return)
            rep="n"
        ;;
    esac
}
vagrant_menu(){
    while true; do
        title_vagrant_menu

        echo -e "\n${GREEN}Choose a project:${NC}\n"
        PS3=$(echo -e "\n${GREEN}Input your choice:${NC} ")

        select project in "${projects[@]}"; do
            if [ "$project" == "Return" ]; then
                clear
                break 2

            elif [ "$project" == "elk" ]; then
                clear
                title_vagrant_menu

                echo -e "\n${GREEN}Choose a project:${NC}\n"
                select project in "${projects_elk[@]}"; do
                    if [ "$project" == "Return" ]; then
                        clear
                        break 3
                    elif [[ -n $project ]]; then
                        clear
                        title_vagrant_menu
                        vagrant_command "$project"
                        if [ "$rep" == "y" ]; then
                            break 3
                        else
                            clear
                            break 2
                        fi
                    else
                        echo -e "Invalid project."
                    fi
                done

            elif [ "$project" == "bastion" ]; then
                clear
                title_vagrant_menu

                echo -e "\n${GREEN}Choose a project:${NC}\n"
                select project in "${projects_bastion[@]}"; do
                    if [ "$project" == "Return" ]; then
                        clear
                        break 3
                    elif [[ -n $project ]]; then
                        clear
                        title_vagrant_menu
                        vagrant_command "$project"
                        if [ "$rep" == "y" ]; then
                            break 3
                        else
                            clear
                            break 2
                        fi
                    else
                        echo -e "Invalid project."
                    fi
                done

            elif [ "$project" == "sql" ]; then
                clear
                title_vagrant_menu

                echo -e "\n${GREEN}Choose a project:${NC}\n"
                select project in "${projects_sql[@]}"; do
                    if [ "$project" == "Return" ]; then
                        clear
                        break 3
                    elif [[ -n $project ]]; then
                        clear
                        title_vagrant_menu
                        vagrant_command "$project"
                        if [ "$rep" == "y" ]; then
                            break 3
                        else
                            clear
                            break 2
                        fi
                    else
                        echo -e "Invalid project."
                    fi
                done

            elif [ "$project" == "haproxy" ]; then
                clear
                title_vagrant_menu

                echo -e "\n${GREEN}Choose a project:${NC}\n"
                select project in "${projects_haproxy[@]}"; do
                    if [ "$project" == "Return" ]; then
                        clear
                        break 3
                    elif [[ -n $project ]]; then
                        clear
                        title_vagrant_menu
                        vagrant_command "$project"
                        if [ "$rep" == "y" ]; then
                            break 3
                        else
                            clear
                            break 2
                        fi
                    else
                        echo -e "Invalid project."
                    fi
                done

            elif [ "$project" == "samba" ]; then
                clear
                title_vagrant_menu

                echo -e "\n${GREEN}Choose a project:${NC}\n"
                select project in "${projects_samba[@]}"; do
                    if [ "$project" == "Return" ]; then
                        clear
                        break 3
                    elif [[ -n $project ]]; then
                        clear
                        title_vagrant_menu
                        vagrant_command "$project"
                        if [ "$rep" == "y" ]; then
                            break 3
                        else
                            clear
                            break 2
                        fi
                    else
                        echo -e "Invalid project."
                    fi
                done

            elif [ "$project" == "monitoring" ]; then
                clear
                title_vagrant_menu

                echo -e "\n${GREEN}Choose a project:${NC}\n"
                select project in "${projects_monitoring[@]}"; do
                    if [ "$project" == "Return" ]; then
                        clear
                        break 3
                    elif [[ -n $project ]]; then
                        clear
                        title_vagrant_menu
                        vagrant_command "$project"
                        if [ "$rep" == "y" ]; then
                            break 3
                        else
                            clear
                            break 2
                        fi
                    else
                        echo -e "Invalid project."
                    fi
                done    

            elif [[ -n $project ]]; then
                clear
                echo "+--------------+"
                echo -e "| ${CYAN}Menu Vagrant${NC} |"
                echo "+--------------+"
                vagrant_command "$project"
            else
                echo -e "Invalid project."
            fi
        done
    done
}

### SSH ###
ssh_copy_influxdb(){
    TARGET_HOST="192.168.56.19"
    TARGET_USER="debian"
    # Copie de la clé publique vers la machine cible
    ssh-copy-id -i ~/.ssh/id_ed25519.pub ${TARGET_USER}@${TARGET_HOST}
}
ssh_copy_grafana(){
    TARGET_HOST="192.168.56.20"
    TARGET_USER="debian"
    # Copie de la clé publique vers la machine cible
    ssh-copy-id -i ~/.ssh/id_ed25519.pub ${TARGET_USER}@${TARGET_HOST}
}
ssh_copy_glpi(){
    TARGET_HOST="192.168.56.21"
    TARGET_USER="debian"
    # Copie de la clé publique vers la machine cible
    ssh-copy-id -i ~/.ssh/id_ed25519.pub ${TARGET_USER}@${TARGET_HOST}
}
ssh_copy_guacamole(){
    TARGET_HOST="192.168.56.22"
    TARGET_USER="debian"
    # Copie de la clé publique vers la machine cible
    ssh-copy-id -i ~/.ssh/id_ed25519.pub ${TARGET_USER}@${TARGET_HOST}
}
ssh_copy_haproxy_1(){
    TARGET_HOST="192.168.56.23"
    TARGET_USER="debian"
    # Copie de la clé publique vers la machine cible
    ssh-copy-id -i ~/.ssh/id_ed25519.pub ${TARGET_USER}@${TARGET_HOST}
}
ssh_copy_haproxy_2(){
    TARGET_HOST="192.168.56.24"
    TARGET_USER="debian"
    # Copie de la clé publique vers la machine cible
    ssh-copy-id -i ~/.ssh/id_ed25519.pub ${TARGET_USER}@${TARGET_HOST}
}
ssh_copy_maria_1(){
    TARGET_HOST="192.168.56.25"
    TARGET_USER="debian"
    # Copie de la clé publique vers la machine cible
    ssh-copy-id -i ~/.ssh/id_ed25519.pub ${TARGET_USER}@${TARGET_HOST}
}
ssh_copy_maria_2(){
    TARGET_HOST="192.168.56.26"
    TARGET_USER="debian"
    # Copie de la clé publique vers la machine cible
    ssh-copy-id -i ~/.ssh/id_ed25519.pub ${TARGET_USER}@${TARGET_HOST}
}
ssh_copy_proxy_squid(){
    TARGET_HOST="192.168.56.27"
    TARGET_USER="debian"
    # Copie de la clé publique vers la machine cible
    ssh-copy-id -i ~/.ssh/id_ed25519.pub ${TARGET_USER}@${TARGET_HOST}
}
ssh_copy_samba_1(){
    TARGET_HOST="192.168.56.28"
    TARGET_USER="debian"
    # Copie de la clé publique vers la machine cible
    ssh-copy-id -i ~/.ssh/id_ed25519.pub ${TARGET_USER}@${TARGET_HOST}
}
ssh_copy_samba_2(){
    TARGET_HOST="192.168.56.29"
    TARGET_USER="debian"
    # Copie de la clé publique vers la machine cible
    ssh-copy-id -i ~/.ssh/id_ed25519.pub ${TARGET_USER}@${TARGET_HOST}
}
ssh_copy_elasticsearch_1(){
    TARGET_HOST="192.168.56.30"
    TARGET_USER="debian"
    # Copie de la clé publique vers la machine cible
    ssh-copy-id -i ~/.ssh/id_ed25519.pub ${TARGET_USER}@${TARGET_HOST}
}
ssh_copy_elasticsearch_2(){
    TARGET_HOST="192.168.56.40"
    TARGET_USER="debian"
    # Copie de la clé publique vers la machine cible
    ssh-copy-id -i ~/.ssh/id_ed25519.pub ${TARGET_USER}@${TARGET_HOST}
}
ssh_copy_kibana(){
    TARGET_HOST="192.168.56.31"
    TARGET_USER="debian"
    # Copie de la clé publique vers la machine cible
    ssh-copy-id -i ~/.ssh/id_ed25519.pub ${TARGET_USER}@${TARGET_HOST}
}
ssh_copy_teleport(){
    TARGET_HOST="192.168.56.32"
    TARGET_USER="debian"
    # Copie de la clé publique vers la machine cible
    ssh-copy-id -i ~/.ssh/id_ed25519.pub ${TARGET_USER}@${TARGET_HOST}
}
clear_ssh_known_hosts(){
    echo -e "${YELLOW}clear known host in progress${NC}"
    sleep 1
    echo "" > ~/.ssh/known_hosts
    printf "${GREEN}successful${NC}"
    sleep 1
    clear
}
title_ssh_menu () {
    clear
    echo "+----------+"
    echo -e "| ${CYAN}Menu SSH${NC} |"
    echo "+----------+"
}
ssh_menu(){
    while true; do
        title_ssh_menu
        echo -e "\n${GREEN}Choice:${NC}\n"
        echo "1) clear known hosts"
        echo "2) ssh copy"
        echo "3) return"

        printf "\n${GREEN}Input Your choice:${NC} "
        read choice
        case $choice in
            1|clear_known_hosts)
                clear
                clear_ssh_known_hosts
            ;;    
            2|ssh_copy)
                title_ssh_menu
                echo -e "\n${GREEN}Choose a project:${NC}\n"
                PS3=$(echo -e "\n${GREEN}Input your choice:${NC} ")

                select project in "${projects[@]}"; do
                    if [ "$project" == "Return" ]; then
                        clear
                        break 2

                    elif [ "$project" == "elk" ]; then
                        clear
                        title_ssh_menu
                        echo -e "\n${GREEN}Choose a project:${NC}\n"
                        select project in "${projects_elk[@]}"; do
                            if [ "$project" == "Return" ]; then
                                clear
                                break 3
                            elif [[ -n $project ]]; then
                                clear
                                ssh_copy_$project
                                return_menu
                                if [ "$rep" == "y" ]; then
                                    break 3
                                else
                                    break 2
                                fi
                            else
                                echo -e "Invalid project."
                            fi
                        done

                    elif [ "$project" == "bastion" ]; then
                        clear
                        title_ssh_menu
                        echo -e "\n${GREEN}Choose a project:${NC}\n"
                        select project in "${projects_bastion[@]}"; do
                            if [ "$project" == "Return" ]; then
                                clear
                                break 3
                            elif [[ -n $project ]]; then
                                clear
                                ssh_copy_$project
                                return_menu
                                if [ "$rep" == "y" ]; then
                                    break 3
                                else
                                    break 2
                                fi
                            else
                                echo -e "Invalid project."
                            fi
                        done

                    elif [ "$project" == "sql" ]; then
                        clear
                        title_ssh_menu
                        echo -e "\n${GREEN}Choose a project:${NC}\n"
                        select project in "${projects_sql[@]}"; do
                            if [ "$project" == "Return" ]; then
                                clear
                                break 3
                            elif [[ -n $project ]]; then
                                clear
                                ssh_copy_$project
                                return_menu
                                if [ "$rep" == "y" ]; then
                                    break 3
                                else
                                    break 2
                                fi
                            else
                                echo -e "Invalid project."
                            fi
                        done

                    elif [ "$project" == "haproxy" ]; then
                        clear
                        title_ssh_menu
                        echo -e "\n${GREEN}Choose a project:${NC}\n"
                        select project in "${projects_haproxy[@]}"; do
                            if [ "$project" == "Return" ]; then
                                clear
                                break 3
                            elif [[ -n $project ]]; then
                                clear
                                ssh_copy_$project
                                return_menu
                                if [ "$rep" == "y" ]; then
                                    break 3
                                else
                                    break 2
                                fi
                            else
                                echo -e "Invalid project."
                            fi
                        done

                    elif [ "$project" == "samba" ]; then
                        clear
                        title_ssh_menu
                        echo -e "\n${GREEN}Choose a project:${NC}\n"
                        select project in "${projects_samba[@]}"; do
                            if [ "$project" == "Return" ]; then
                                clear
                                break 3
                            elif [[ -n $project ]]; then
                                clear
                                ssh_copy_$project
                                return_menu
                                if [ "$rep" == "y" ]; then
                                    break 3
                                else
                                    break 2
                                fi
                            else
                                echo -e "Invalid project."
                            fi
                        done

                    elif [ "$project" == "monitoring" ]; then
                        clear
                        title_ssh_menu
                        echo -e "\n${GREEN}Choose a project:${NC}\n"
                        select project in "${projects_monitoring[@]}"; do
                            if [ "$project" == "Return" ]; then
                                clear
                                break 3
                            elif [[ -n $project ]]; then
                                clear
                                ssh_copy_$project
                                return_menu
                                if [ "$rep" == "y" ]; then
                                    break 3
                                else
                                    break 2
                                fi
                            else
                                echo -e "Invalid project."
                            fi
                        done

                    elif [[ -n $project ]]; then
                        clear
                        ssh_copy_$project
                        return_menu
                        if [ "$rep" == "y" ]; then
                            break 3
                        else
                            break 2
                        fi
                    else
                        echo -e "Invalid project."
                    fi
                done
            ;;
            3|return)
                clear
                break 1
            ;;
        esac
    done
}

### Ansible ###
run_playbook_elasticsearch_1() {
    ansible-playbook -i $parent_directory/Ansible/inventory $parent_directory/Ansible/playbook/elasticsearch.yml
}
run_playbook_elasticsearch_2() {
    ansible-playbook -i $parent_directory/Ansible/inventory $parent_directory/Ansible/playbook/elasticsearch.yml
}
run_playbook_kibana() {
    ansible-playbook -i $parent_directory/Ansible/inventory $parent_directory/Ansible/playbook/kibana.yml
}
run_playbook_grafana() {
    ansible-playbook -i $parent_directory/Ansible/inventory $parent_directory/Ansible/playbook/grafana.yml
}
run_playbook_influxdb() {
    ansible-playbook -i $parent_directory/Ansible/inventory $parent_directory/Ansible/playbook/influxdb.yml
}
run_playbook_samba_1() {
    ansible-playbook -i $parent_directory/Ansible/inventory $parent_directory/Ansible/playbook/samba_1.yml
}
run_playbook_samba_2() {
    ansible-playbook -i $parent_directory/Ansible/inventory $parent_directory/Ansible/playbook/samba_2.yml
}
run_playbook_proxy_squid() {
    ansible-playbook -i $parent_directory/Ansible/inventory $parent_directory/Ansible/playbook/proxy_squid.yml
}
run_playbook_maria_1() {
    ansible-playbook -i $parent_directory/Ansible/inventory $parent_directory/Ansible/playbook/maria_1.yml
}
run_playbook_maria_2() {
    ansible-playbook -i $parent_directory/Ansible/inventory $parent_directory/Ansible/playbook/maria_2.yml
}
run_playbook_haproxy_1() {
    ansible-playbook -i $parent_directory/Ansible/inventory $parent_directory/Ansible/playbook/haproxy_1.yml
}
run_playbook_haproxy_2() {
    ansible-playbook -i $parent_directory/Ansible/inventory $parent_directory/Ansible/playbook/haproxy_2.yml
}
run_playbook_guacamole() {
    ansible-playbook -i $parent_directory/Ansible/inventory $parent_directory/Ansible/playbook/guacamole.yml
}
run_playbook_teleport() {
    ansible-playbook -i $parent_directory/Ansible/inventory $parent_directory/Ansible/playbook/teleport.yml
}
run_playbook_glpi() {
    ansible-playbook -i $parent_directory/Ansible/inventory $parent_directory/Ansible/playbook/glpi.yml
}
title_ansible_menu() {
    echo "+--------------+"
    echo -e "| ${CYAN}Menu Ansible${NC} |"
    echo "+--------------+"
}
ansible_menu(){
    while true; do
        title_ansible_menu
        echo -e "\n${GREEN}Choose a project:${NC}\n"
        PS3=$(echo -e "\n${GREEN}Input your choice:${NC} ")

        select project in "${projects[@]}"; do
            if [ "$project" == "Return" ]; then
                clear
                break 2
            elif [ "$project" == "elk" ]; then
                clear
                title_ansible_menu
                echo -e "\n${GREEN}Choose a project:${NC}\n"
                select project in "${projects_elk[@]}"; do
                    if [ "$project" == "Return" ]; then
                        clear
                        break 3
                    elif [[ -n $project ]]; then
                        clear
                        run_playbook_$project
                        return_menu
                        if [ "$rep" == "y" ]; then
                            break 3
                        else
                            break 2
                        fi
                    else
                        echo -e "Invalid project."
                    fi
                done

            elif [ "$project" == "bastion" ]; then
                clear
                title_ansible_menu
                echo -e "\n${GREEN}Choose a project:${NC}\n"
                select project in "${projects_bastion[@]}"; do
                    if [ "$project" == "Return" ]; then
                        clear
                        break 3
                    elif [[ -n $project ]]; then
                        clear
                        run_playbook_$project
                        return_menu
                        if [ "$rep" == "y" ]; then
                            break 3
                        else
                            break 2
                        fi
                    else
                        echo -e "Invalid project."
                    fi
                done

            elif [ "$project" == "sql" ]; then
                clear
                title_ansible_menu
                echo -e "\n${GREEN}Choose a project:${NC}\n"
                select project in "${projects_sql[@]}"; do
                    if [ "$project" == "Return" ]; then
                        clear
                        break 3
                    elif [[ -n $project ]]; then
                        clear
                        run_playbook_$project
                        return_menu
                        if [ "$rep" == "y" ]; then
                            break 3
                        else
                            break 2
                        fi
                    else
                        echo -e "Invalid project."
                    fi
                done

            elif [ "$project" == "haproxy" ]; then
                clear
                title_ansible_menu
                echo -e "\n${GREEN}Choose a project:${NC}\n"
                select project in "${projects_haproxy[@]}"; do
                    if [ "$project" == "Return" ]; then
                        clear
                        break 3
                    elif [[ -n $project ]]; then
                        clear
                        run_playbook_$project
                        return_menu
                        if [ "$rep" == "y" ]; then
                            break 3
                        else
                            break 2
                        fi
                    else
                        echo -e "Invalid project."
                    fi
                done

            elif [ "$project" == "samba" ]; then
                clear
                title_ansible_menu
                echo -e "\n${GREEN}Choose a project:${NC}\n"
                select project in "${projects_samba[@]}"; do
                    if [ "$project" == "Return" ]; then
                        clear
                        break 3
                    elif [[ -n $project ]]; then
                        clear
                        run_playbook_$project
                        return_menu
                        if [ "$rep" == "y" ]; then
                            break 3
                        else
                            break 2
                        fi
                    else
                        echo -e "Invalid project."
                    fi
                done

            elif [ "$project" == "monitoring" ]; then
                clear
                title_ansible_menu
                echo -e "\n${GREEN}Choose a project:${NC}\n"
                select project in "${projects_monitoring[@]}"; do
                    if [ "$project" == "Return" ]; then
                        clear
                        break 3
                    elif [[ -n $project ]]; then
                        clear
                        run_playbook_$project
                        return_menu
                        if [ "$rep" == "y" ]; then
                            break 3
                        else
                            break 2
                        fi
                    else
                        echo -e "Invalid project."
                    fi
                done 

            elif [[ -n $project ]]; then
                clear
                run_playbook_$project
                return_menu
                if [ "$rep" == "y" ]; then
                    break 3
                else
                    break 2
                fi
            else
                echo -e "Invalid project."
            fi
        done
    done
}

########
# MAIN #
########
while true; do
    clear
    print_main_menu
done
    